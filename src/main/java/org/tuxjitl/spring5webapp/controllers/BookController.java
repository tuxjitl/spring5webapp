package org.tuxjitl.spring5webapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.tuxjitl.spring5webapp.repositories.BookRepository;

@Controller
public class BookController {

    private BookRepository bookRepository;

    public BookController(BookRepository bookRepository) {

        this.bookRepository = bookRepository;
    }

    @RequestMapping("/books")
    public String getBooks(Model model){

        //add a list of books out of the repository to the model
        model.addAttribute("books",bookRepository.findAll());

        //for thymeleaf use: associate this string with the thymeleaf view called books
        return "books";

    }
}
