package org.tuxjitl.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;
import org.tuxjitl.spring5webapp.model.Author;

public interface AuthorRepository extends CrudRepository<Author,Long> {


}
