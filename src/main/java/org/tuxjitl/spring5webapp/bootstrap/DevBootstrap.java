package org.tuxjitl.spring5webapp.bootstrap;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.tuxjitl.spring5webapp.model.Author;
import org.tuxjitl.spring5webapp.model.Book;
import org.tuxjitl.spring5webapp.model.Publisher;
import org.tuxjitl.spring5webapp.repositories.AuthorRepository;
import org.tuxjitl.spring5webapp.repositories.BookRepository;
import org.tuxjitl.spring5webapp.repositories.PublisherRepository;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;
    private  PublisherRepository publisherRepository;

    public DevBootstrap(AuthorRepository authorRepository, BookRepository bookRepository,
                        PublisherRepository publisherRepo) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepo;
    }
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }
    private void initData(){
        Publisher aPress = new Publisher("APress"," main street boston");
        Publisher pearson = new Publisher("Pearson","1145 Wall street New York");
        publisherRepository.save(aPress);
        publisherRepository.save(pearson);

        //sandra
        Author sandra = new Author("Sandra","Janssens");
        Book aBook = new Book("Disappearance","1111",aPress);
        sandra.getBooks().add(aBook);
        aBook.getAuthors().add(sandra);

        authorRepository.save(sandra);
        bookRepository.save(aBook);

        //mieke
        Author mieke = new Author("mieke","Celis");
        Book bBook = new Book("The fall","1111",pearson);
        mieke.getBooks().add(bBook);
        bBook.getAuthors().add(mieke);

        authorRepository.save(mieke);
        bookRepository.save(bBook);

    }


}
